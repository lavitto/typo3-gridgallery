<?php

return [
    'ext-gridgallery-gallery' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:gridgallery/Resources/Public/Icons/ContentElements/gridgallery_gallery.svg',
    ],
];
